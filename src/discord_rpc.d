module discord_rpc;

import discord_register;

import std.stdio : writeln;

alias int8_t  = byte;
alias int64_t = long;

version(SL_DISCORD_RPC) {} else
static this()
{
    import dynalib;
    import discord_rpc_conditionals;
    
    DynaLib lib;
    if (!lib.loadFromConfig("discord-rpc", "discord"))
    {
        version(DL_ERROR)
            writeln("There was a problem loading discord-rpc");
        return;
    }
    
    // Register
    lib.bind(&Discord_Register, "Discord_Register");
    lib.bind(&Discord_RegisterSteamGame, "Discord_RegisterSteamGame");
    
    // RPC
    lib.bind(&Discord_Initialize, "Discord_Initialize");
    lib.bind(&Discord_Shutdown, "Discord_Shutdown");
    lib.bind(&Discord_RunCallbacks, "Discord_RunCallbacks");
    
    if (lib.bind(&Discord_UpdateConnection, "Discord_UpdateConnection"))
    {
        version(DL_NOTIFICATION)
            writeln("Dynalib-Conditional: [Discord-RPC] Disable-IO-Thread active.");
        (cast(bool*)&conditional_DisableIOThread)[0] = true;
    } else (cast(bool*)&conditional_DisableIOThread)[0] = false;
    
    lib.bind(&Discord_UpdatePresence, "Discord_UpdatePresence");
    lib.bind(&Discord_ClearPresence, "Discord_ClearPresence");
    lib.bind(&Discord_Respond, "Discord_Respond");
    lib.bind(&Discord_UpdateHandlers, "Discord_UpdateHandlers");
}

@nogc nothrow __gshared extern(C):
    struct DiscordRichPresence
    {
        const(char)* state;   /* max 128 bytes */
        const(char)* details; /* max 128 bytes */
        int64_t startTimestamp;
        int64_t endTimestamp;
        const(char)* largeImageKey;  /* max 32 bytes */
        const(char)* largeImageText; /* max 128 bytes */
        const(char)* smallImageKey;  /* max 32 bytes */
        const(char)* smallImageText; /* max 128 bytes */
        const(char)* partyId;        /* max 128 bytes */
        int partySize;
        int partyMax;
        int partyPrivacy;
        const(char)* matchSecret;    /* max 128 bytes */
        const(char)* joinSecret;     /* max 128 bytes */
        const(char)* spectateSecret; /* max 128 bytes */
        int8_t instance;
    }

    struct DiscordUser
    {
        const(char)* userId;
        const(char)* username;
        const(char)* discriminator;
        const(char)* avatar;
    }

    struct DiscordEventHandlers
    {
        @nogc nothrow __gshared extern(C):
            void function(const(DiscordUser)* request)         ready;
            void function(int errorCode, const(char)* message) disconnected;
            void function(int errorCode, const(char)* message) errored;
            void function(const(char)* joinSecret)             joinGame;
            void function(const(char)* spectateSecret)         spectateGame;
            void function(const(DiscordUser)* request)         joinRequest;
    }

    enum DISCORD_REPLY_NO = 0;
    enum DISCORD_REPLY_YES = 1;
    enum DISCORD_REPLY_IGNORE = 2;
    enum DISCORD_REPLY_PRIVATE = 0;
    enum DISCORD_REPLY_PUBLIC = 1;

version(SL_DISCORD_RPC)
{
    void Discord_Initialize(const(char)* applicationId,
                            DiscordEventHandlers* handlers,
                            int autoRegister,
                            const(char)* optionalSteamId);
    
    void Discord_Shutdown();
    
    /* checks for incoming messages, dispatches callbacks */
    void Discord_RunCallbacks();

    /* If you disable the lib starting its own io thread, you'll need to call this from your own */
    version (DISCORD_DISABLE_IO_THREAD)
        void Discord_UpdateConnection();

    void Discord_UpdatePresence(const DiscordRichPresence* presence);
    
    void Discord_ClearPresence();

    void Discord_Respond(const(char)* userid, /* DISCORD_REPLY_ */ int reply);

    void Discord_UpdateHandlers(DiscordEventHandlers* handlers);
}
else
{
    void function(const(char)* applicationId,
                  DiscordEventHandlers* handlers,
                  int autoRegister,
                  const(char)* optionalSteamId) Discord_Initialize;
    
    void function() Discord_Shutdown;
    
    /* checks for incoming messages, dispatches callbacks */
    void function() Discord_RunCallbacks;

    /* If you disable the lib starting its own io thread, you'll need to call this from your own */
    void function() Discord_UpdateConnection;

    void function(const DiscordRichPresence* presence) Discord_UpdatePresence;
    
    void function() Discord_ClearPresence;

    void function(const(char)* userid, /* DISCORD_REPLY_ */ int reply) Discord_Respond;

    void function(DiscordEventHandlers* handlers) Discord_UpdateHandlers;
}