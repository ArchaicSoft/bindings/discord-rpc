module discord_register;

@nogc nothrow __gshared extern(C):

version(SL_DISCORD_RPC)
{
    void Discord_Register(const(char)* applicationId, const(char)* command);
    void Discord_RegisterSteamGame(const(char)* applicationId, const(char)* steamId);
}
else
{
    void function(const(char)* applicationId, const(char)* command) Discord_Register;
    void function(const(char)* applicationId, const(char)* steamId) Discord_RegisterSteamGame;
}