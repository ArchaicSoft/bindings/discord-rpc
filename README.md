## Discord-RPC-D

This is a package for interacting with the official Discord-RPC dynamic library. [v3.4.0]

All interaction operates like its natural C counterpart, and is bound using dynalib to runtime-load conveniently.

Referenced source:
https://github.com/discord/discord-rpc/tree/master/include

Optional:

Clone precompiled binaries and bind configuration file to output directory by adding the sub-package "copy-files" to your dependencies.

Static binding by adding the following line to your dub.
"versions": [ "SL_DISCORD_RPC" ]